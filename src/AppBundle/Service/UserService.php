<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Exception\UserDataValidationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Tests\Encoder\PasswordEncoder;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * @Service()
 */
class UserService
{
    private $em;
    private $validator;
    private $passwordEncoder;

    public function __construct(ContainerInterface $container)
    {
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->validator = $container->get('validator');
        $this->passwordEncoder = $container->get('security.password_encoder');
    }

    public function createOrUpdate(User $user, User $oldUser = null)
    {
        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            $errorsArr = array();
            foreach ($errors as $error) {
                $errorsArr[$error->getPropertyPath()] = $error->getMessage();
            }
            throw new UserDataValidationException($errorsArr);
        }

        $password = $this->passwordEncoder->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($password);

        $userRepo = $this->em->getRepository('AppBundle:User');
        try {
            $this->save($user);
        } catch (UniqueConstraintViolationException $exp) {
            $errorsArr = array();
            if ((!$oldUser || $oldUser->getUsername() !== $user->getUsername()) && $userRepo->findOneByUsername($user->getUsername()) !== null) {
                $errorsArr['username'] = 'This username is already used.';
            }
            if ((!$oldUser || $oldUser->getEmail() !== $user->getEmail()) && $userRepo->findOneByEmail($user->getEmail()) !== null) {
                $errorsArr['email'] = 'This email is already used.';
            }
            throw new UserDataValidationException($errorsArr);
        }

    }

    public function getUser($username, $password)
    {
        $userRepo = $this->em->getRepository('AppBundle:User');

        $user = $userRepo->findOneByUsername($username);
        if ($user === null || !$this->passwordEncoder->isPasswordValid($user, $password)) {
            return null;
        }

        return $user;
    }

    public function delete(User $user)
    {
        $this->em->remove($user);
        $this->em->flush();
    }

    public function generateUserWeekToken(User $user)
    {
        $token = bin2hex(random_bytes(16));
        $user->setToken(hash('sha256', $token));
        $user->setTokenExpireDate((new \DateTime())->modify('+7 days'));

        $this->save($user);
        return $token;
    }

    public function save(User $user) {
        $this->em->persist($user);
        $this->em->flush($user);
    }
}