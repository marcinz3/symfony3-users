<?php

namespace AppBundle\Controller;

use AppBundle\Exception\UserDataValidationException;
use AppBundle\Form\Type\LoginUserType;
use AppBundle\Form\Type\RegisterUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/user-api", defaults={"_format": "json"})
 */
class RestController extends Controller
{
    /**
     * @Route("/create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $newUser = new User();

        try {
            $body = $request->getContent();
            $data = json_decode($body);
            $this->parseUserData($newUser, $data);
        } catch (\Exception $exp) {
            return new JsonResponse(['status' => 'error', 'message' => 'Bad request'], Response::HTTP_BAD_REQUEST);
        }

        $userService = $this->get('user.service');

        try {
            $userService->createOrUpdate($newUser);
            $token = $userService->generateUserWeekToken($newUser);
        } catch (UserDataValidationException $exp) {
            return JsonResponse::fromJsonString($exp->getJsonMessage(), Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse([
            'status' => 'success',
            'token' => $token,
            'tokenExpirationTimestamp' => $newUser->getTokenExpireDate()->getTimestamp()
        ]);
    }

    /**
     * @Route("/login")
     * @Method("POST")
     */
    public function loginAction(Request $request)
    {
        $username = $password = null;

        try {
            $body = $request->getContent();
            $data = json_decode($body);
            $username = $data->username;
            $password = $data->password;
        } catch (\Exception $exp) {
            return new JsonResponse(['status' => 'error', 'message' => 'Bad request'], Response::HTTP_BAD_REQUEST);
        }

        $userService = $this->get('user.service');
        $user = $userService->getUser($username, $password);
        if ($user === null) {
            return new JsonResponse(['status' => 'error', 'message' => 'Invalid credentials'], Response::HTTP_UNAUTHORIZED);
        }
        $token = $userService->generateUserWeekToken($user);

        return new JsonResponse([
            'status' => 'success',
            'token' => $token,
            'tokenExpirationTimestamp' => $user->getTokenExpireDate()->getTimestamp()
        ]);
    }

    /**
     * @Route("/info")
     * @Method("POST")
     */
    public function infoAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();

        return new JsonResponse(['status' => 'success', 'user' => [
            'username' => $user->getUsername(),
            'email' => $user->getEmail()
        ]]);
    }

    /**
     * @Route("/update")
     * @Method("POST")
     */
    public function updateAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $oldUser = clone $user;

        try {
            $body = $request->getContent();
            $data = json_decode($body);
            $this->parseUserData($user, $data);
        } catch (\Exception $exp) {
            return new JsonResponse(['status' => 'error', 'message' => 'Bad request'], Response::HTTP_BAD_REQUEST);
        }

        $userService = $this->get('user.service');
        try {
            $userService->createOrUpdate($user, $oldUser);
        } catch (UserDataValidationException $exp) {
            return JsonResponse::fromJsonString($exp->getJsonMessage(), Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['status' => 'success']);
    }

    /**
     * @Route("/delete")
     * @Method("POST")
     */
    public function deleteAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $username = null;

        try {
            $body = $request->getContent();
            $data = json_decode($body);
            $username = $data->username;
        } catch (\Exception $exp) {
            return new JsonResponse(['status' => 'error', 'message' => 'Bad request'], Response::HTTP_BAD_REQUEST);
        }

        if ($user->getUsername() !== $username) {
            return new JsonResponse(['status' => 'error', 'message' => 'Bad request'], Response::HTTP_BAD_REQUEST);
        }

        $this->get('user.service')->delete($user);

        return new JsonResponse(['status' => 'success']);
    }


    private function parseUserData(User $user, $data)
    {
        $user->setUsername($data->username);
        $user->setEmail($data->email);
        $user->setPlainPassword($data->password);
    }
}
