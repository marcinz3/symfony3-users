<?php

namespace AppBundle\Controller;

use AppBundle\Exception\UserDataValidationException;
use AppBundle\Form\Type\LoginUserType;
use AppBundle\Form\Type\RegisterUserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/logowanie", name="login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authUtils)
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        $loginForm = $this->createForm(LoginUserType::class, array(
            'username' => $lastUsername
        ));

        return $this->render(':default:login.html.twig', array(
            'form' => $loginForm->createView(),
            'error' => $error,
        ));
    }

    /**
     * @Route(
     *      "/rejestracja/{id}",
     *      name="userRegisterForm",
     *      requirements={"id"="\d+"},
     *      defaults={"id"=NULL, "city"=null}
     * )
     *
     */
    public function registerFormAction(Request $Request, User $user = null)
    {
        if (null == $user) {
            $user = new User();
        }

        $form = $this->createForm(RegisterUserType::class, $user);

        $form->handleRequest($Request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userService = $this->get('user.service');

            try {
                $userService->createOrUpdate($user);

                $this->get('session')->getFlashBag()->add('success', 'Twoje konto zostało utworzone - zaloguj się!');
                return $this->redirectToRoute('homepage');
            } catch (UserDataValidationException $exp) {
                foreach ($exp->getErrorFieldsArr() as $field => $error) {
                    $form->get($field)->addError(new FormError($error));
                }
            }
        }

        return $this->render('default/register.html.twig', array(
            'form' => $form->createView(),
            'user' => $user
        ));
    }

}
