<?php

namespace AppBundle\Exception;

class UserDataValidationException extends \Exception
{
    private $errorFieldsArr;

    /**
     * UserDataValidationException constructor.
     * @param string $errorFieldsArr
     */
    public function __construct($errorFieldsArr)
    {
        $this->errorFieldsArr = $errorFieldsArr;
    }

    /**
     * @return string
     */
    public function getJsonMessage()
    {
        return json_encode(['status' => 'error', 'invalidFields' => $this->errorFieldsArr]);
    }

    /**
     * @return string
     */
    public function getErrorFieldsArr()
    {
        return $this->errorFieldsArr;
    }
}
