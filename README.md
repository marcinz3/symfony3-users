# Symfony3 users #

Prosta aplikacja umożliwiająca tworzenie kont i logowanie oparta na Symfony3 Framework bez korzystania z "zewnętrznych" bundli.

Front-end framework: MaterializeCSS.

## Rest API ##
### Tworzenie konta użytkownika: ###

* Żądanie:

```
POST /user-api/create
{
  "username": "user1",
  "email": "user1@gmail.com",
  "password": "12345678"
}
```

* W przypadku powodzenia generowany jest token np:

```
{
    "status": "success",
    "token": "db2b03bafdc77414136c06fd3fea4fe5",
    "tokenExpirationTimestamp": 1499682690
}
```
* W przypadku błędu np. za krótka nazwa użytkownika:

```
{
    "status": "error",
    "invalidFields": {
        "username": "This value is too short. It should have 5 characters or more."
    }
}
```
### Logowanie ###
* Żądanie

```
POST /user-api/login
{
  "username": "user2",
  "password": "212345678"
}

```
* W przypadku powodzenia generowany jest token np:

```
{
    "status": "success",
    "token": "4a79586c56b50662dcebd8cfe2a4eaac",
    "tokenExpirationTimestamp": 1499683327
}
```
* W przypadku błędnych danych logowania:

```
{
    "status": "error",
    "message": "Invalid credentials"
}
```
### Pobieranie danych użytkownika ###
* Żądanie

```
POST /user-api/info
```
* Przykładowa odpowiedź:

```
{
    "status": "success",
    "user": {
        "username": "user1",
        "email": "user1@gmail.com"
    }
}
```
### Edycja danych użytkownika ###
* Żądanie

```
{
  "username": "user1b",
  "email": "user1b@gmail.com",
  "password": "b12345678"
}
```
* W przypadku powodzenia:

```
{
    "status": "success"
}
```
* W przypadku błędu (tak jak przy tworzeniu) np:

```
{
    "status": "error",
    "invalidFields": {
        "email": "This email is already used."
    }
}
```
### Usuwanie swojego konta ###
* Żądanie

```
POST /user-api/delete
{
  "username": "user1"
}
```
* W przypadku powodzenia:

```
{
    "status": "success"
}
```
* W przypadku niezgodności tokenu z podaną nazwą użytkownika:

```
{
    "status": "error",
    "message": "Bad request"
}
```

### Uwaga! - pobranie danych / edycja / usunięcie ###
Wymaga podania tokenu w nagłówku **X-AUTH-TOKEN**.

Token jest tworzony w momencie tworzenia konta i ma tygodniową ważność.
Nowy token można wygenerować za pomocą logowania.

* W przypadku jego braku, błędnego tokenu lub gdy token wygasł:

```
{
    "status": "error",
    "message": "Invalid token"
}
```